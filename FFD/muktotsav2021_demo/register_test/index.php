<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$fname = '';
$lname = '';
$emailid = '';
$mobile = '';
$state = 0;
$city = 0;
$country = 0;
$speciality = '';
$speciality1 = '';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name is required';
    }
    if (empty($_POST['lname'])) {
        $errors['lname'] = 'Last Name is required';
    }
    if (empty($_POST['emailid'])) {
        $errors['email'] = 'Email ID is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mobile'] = 'Phone No. is required';
    }
    if ($_POST['country'] == '0') {
        $errors['country'] = 'country is required';
    }
    if ($_POST['state'] == '0') {
        $errors['state'] = 'State is required';
    }
    // if ($_POST['city'] == '0') {
    //     $errors['city'] = 'City is required';
    // }
    if ($_POST['speciality'] == '0') {
        $errors['spec'] = 'Speciality is required';
    }
    if ($_POST['speciality1'] == '0') {
        $errors['spec'] = 'Speciality is required';
    }

    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $emailid = $_POST['emailid'];
    $mobile = $_POST['mobile'];
    $speciality = $_POST['speciality'];
    $speciality1 = $_POST['speciality1'];
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    // if (isset($_POST['state'])) {
    //     $state = $_POST['state'];
    // }
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }

    if (count($errors) == 0) {
        $newuser = new User();

        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);
        $newuser->__set('country', $country);
        $newuser->__set('state', $state);
        $newuser->__set('city', $city);
        $newuser->__set('updates', $speciality);
        $newuser->__set('speciality', $speciality1);
        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];

        if ($reg_status == "success") {
            $succ = $add['message'];
            $fname = '';
            $lname = '';
            $emailid = '';
            $mobile = '';
            $speciality = '';
            $country = 0;
            $state = 0;
            $city = 0;
        } else {
            $errors['reg'] = $add['message'];
        }
    }
}

?>

<style>


            .imgwel1 {
                width: 50%;
                margin-top: 358px;
                margin-left: 180px;
                margin-bottom: 20px;
            }

            #form1 {
                /* position: absolute;
                top: 444px;
                left: 700px; */
                width: 100%;
                background-color: #ffffff;
                color: black;
                border-radius: 0px 0px 20px 20px;
                margin-bottom: 4%;
            }
            label {
                font-size: 15px !important;
                font-weight: 1 !important;
            }
            .lebel{
                margin-left:4px;
            }

            .legend {
                border-bottom: 5px solid skyblue;
                width: 93% !important;
                color: darkblue;
                font-size: 22px;
                margin-left: 20px;
                padding-top:12px;
                font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
                padding-left: 166px;
            }

            .btn1 {
                background-image: linear-gradient(to right, #0088c8, #00acd9, #0088c8);
                color: white;
            }

            .btn2 {
                background-color: #ec3739;
                color: white;
            }

            .mob {
                font-size: 16px;
            }

            .email {
                margin-left: 11px;
                width: 96.2%;
            }

            .program {
                margin-left: 7px;
                width: 98%;
            }

            .about {
                margin-left: 7px;
                width: 98%;
            }

            .input{
                width:50%
            }

            .mg {
                padding-left: 5px;
                padding-right: 5px;
            }


            .btn {
                margin: 6px;
                margin-left: 5px;
            }

            .logo {
                /* width: 80%; */
                height: 37px;
            }

            .bg-image {
                background-image: url('assets/images/bgffdmuk.png');
                height: 100vh;
                background-position: top center;
                background-repeat: no-repeat;
                background-size: 65%;
                /* font-size: 14px; */
                /* margin-top:8px; */
            }

            @media only screen and (max-width: 1440px) {
                .bg-image {
                    background-size: 68%;
                }
            }

            @media only screen and (max-width: 1024px) {
                .imgwel1 {
                    width: 63%;
                    margin-top: 292px;
                    margin-left: 98px;
                    margin-bottom: 13px;
                }
                .bg-image {
                    background-size: 67%;
                }
                /* body {
                    background-color: #000044;
                    background-image: url('');
                } */
                .btn1 {
                    font-size: 13.5px;
                }
                .btn2 {
                    font-size: 11.55px;
                }
                .legend {
                    padding-left: 96px ;
                }
            }

            @media only screen and (max-width: 768px) {
                /* body {
                    background-color: #000044;
                    background-image: url('');
                } */
                .imgwel1 {
                    width: 65%;
                    margin-top: 202px;
                    margin-left: 82px;
                    margin-bottom: 17px;
                }
                .legend {
                    /* font-size: 16px; */
                    padding-left: 25px !important;
                    width: 88% !important;
                }
                .bg-image {
                    background-size: 66%;
                }
                #form1 {
                    /* position: absolute;
                    top: 444px;
                    left: 700px; */
                    width: 100%;
                    background-color: #ffffff;
                    color: black;
                    border-radius: 0px 0px 20px 20px;
                }
                .logo {
                    /* width: 80%; */
                    height: 34px;
                }
                .email {
                    width: 93.2%;
                }
                .program {
                    width: 96%;
                }
                .about{
                    width: 96%;
                }
            }

            @media only screen and (max-width: 425px) {
                .imgwel1 {
                    width: 58%;
                    margin-top: 187px;
                    margin-left: 65px;
                    margin-bottom: -7px;
                }
                .bg-image {
                    background-size: 114%;
                }
                /* .form-group {
                    margin-bottom: 0px !important;
                } */
                /* * {
                    font-size: 10px;
                } */
                /* legend {
                    font-size: 14px;
                } */
                .logo {
                    height: 37px !important;
                    width: 200px !important;
                }
            }

            @media only screen and (max-width: 375px) {
                .bg-image {
                    background-size: 127%;
                }
                .legend {
                    /* font-size: 15px; */
                    padding-left: 50px;
                }
            .imgwel1 {
                width: 63%;
                margin-top: 175px;
                margin-left: 49px;
                margin-bottom: 0px;
            }
            }

            @media only screen and (max-width: 320px) {
                .bg-image {
                    background-size: 140%;
                }
                .legend {
                    /* font-size: 15px; */
                    padding-left: 9px;
                }
                .logo {
                    height: 37px !important;
                    width: 140px !important;
                }
                .imgwel1 {
                    width: 62%;
                    margin-top: 173px;
                    margin-left: 47px;
                    margin-bottom: 0px;
                }
                .reg {
                    background-color: #00089a;
                    height: 48px !important;
                    width: 96px !important;
                    color: white;
                    margin-bottom:4px;
                }
            }
    </style>
    <!doctype html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $event_title ?></title>
        <link rel="stylesheet" href="assets/css/normalize.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/all.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
    </head>

    <body id="reg">
    <div class="container reg-content bg-image">
        <div class="row">
            <div class="col-12 col-lg-9 mx-auto">
                <div class="row pt-4" >
                    <div class="col-4 col-md-3 p-3 text-center">
                        <img src="assets/img/endorsed-by.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-4 col-md-2 offset-md-2 text-center p-3">
                        <img src="assets/img/logo-carbs.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-4 col-md-3 offset-md-2 text-center p-3">
                        <img src="assets/img/logo-aace.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-6 col-md-6 col-lg-4 mx-auto">
                <img src="assets/img/block-your-date.png" class="img-fluid" alt="">
            </div>
        </div>
        <div class="register-wrapper mx-4 p-3 p-md-0">
            <div class="row">
                <div class="col-12 col-md-8 mx-auto p-2">
                <div class="row"> 
                    <div class="col-md-12">
                        <img src="assets/images/welcomecr.png" class="imgwel1">

                    </div>  
                </div>
                    <?php
                    if (count($errors) > 0) : ?>
                        <div class="alert alert-danger alert-msg">
                            <ul class="list-unstyled">
                                <?php foreach ($errors as $error) : ?>
                                    <li>
                                        <?php echo $error; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if ($succ != '') { ?>
                        <div class="alert alert-success alert-msg">
                            <?= $succ ?>
                        </div>
                    <?php } ?>
                    <form method="POST" class="mt-4" id="form1">
                        <legend class="legend m-4">Please fill in the Registration Details</legend>
                        <div class="row mt-2 m-2" >
                            <div class="col-12 col-md-6">
                                <label>FIRST NAME</label>
                                <input type="text" id="fname" name="fname" class="form-control" value="<?php echo $fname; ?>" autocomplete="off">
                            </div>
                            <div class="col-12 col-md-6" >
                                <label>LAST NAME</label>
                                <input type="text" id="lname" name="lname" class="form-control" value="<?php echo $lname; ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mt-3 mb-1 m-2">
                            <div class="col-12">
                                <label>EMAIL ID</label>
                                <input type="email" id="emailid" name="emailid" class="form-control" value="<?php echo $emailid; ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mt-3 mb-1 m-2">
                            <div class="col-12 col-md-6">
                                <label>MOBILE NO.</label>
                                <input type="number" id="mobile" name="mobile" class="form-control" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div>
                            <div class="col-12 col-md-6">
                                <label>COUNTRY</label>
                                <div id="countries">
                                    <select class="form-control" id="country" name="country" value="<?= $country ?>" onChange="updateState()">
                                        <option value="0">Select Country</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row mt-3 mb-1 m-2">
                            <div class="col-12 col-md-6">
                                <label>STATE</label>
                                <div id="states">
                                    <select class="form-control" id="state" name="state" value="<?= $state ?>" onChange="updateCity()">
                                        <option value="0">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <label>CITY</label>
                                <div id="cities">
                                    <select class="form-control" id="city" name="city">
                                        <option value="0">Select City</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3 mb-1 m-2">                          
                            <div class="col-12 col-md-12">
                                <label>HAVE YOU ATTENDED ANY OF THE FFD PROGRAMS ?</label>
                                <div id="cities">
                                    <select class="form-control" id="speciality" name="speciality">
                                        <option value="0">Select</option>
                                        <option value="NOT ATTENDED ANY OF PROGRAMS" <?= $speciality === 'NOT ATTENDED ANY OF PROGRAMS' ? 'selected' : '' ?>>NOT ATTENDED ANY OF PROGRAMS</option>
                                        <option value="FIRST ONLINE CLASS/FOUNDATION" <?= $speciality === 'FIRST ONLINE CLASS/FOUNDATION' ? 'selected' : '' ?>>FIRST ONLINE CLASS/FOUNDATION</option>
                                        <option value="INTENSIVE REVERSAL PROGRAM (IRP)" <?= $speciality === 'INTENSIVE REVERSAL PROGRAM (IRP)' ? 'selected' : '' ?>>INTENSIVE REVERSAL PROGRAM (IRP)</option>
                                        <option value="TRANCENDENTAL RESIDENTIAL PROGRAM (TRP)" <?= $speciality === 'TRANCENDENTAL RESIDENTIAL PROGRAM (TRP)' ? 'selected' : '' ?>>TRANCENDENTAL RESIDENTIAL PROGRAM (TRP)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 m-2">
                                <label>FROM WHERE OR WHOM DID YOU HEAR ABOUT THIS EVENT?</label>
                                <div id="cities">
                                    <select class="form-control" id="speciality1" name="speciality1">
                                        <option value="0">Select</option>
                                        <option value="SOCIAL MEDIA" <?= $speciality1 === 'SOCIAL MEDIA' ? 'selected' : '' ?>>SOCIAL MEDIA</option>
                                        <option value="NEWSPAPER" <?= $speciality1 === 'NEWSPAPER' ? 'selected' : '' ?>>NEWSPAPER</option>
                                        <option value="CURRENT/PAST FFD PARTICIPANT" <?= $speciality1 === 'CURRENT/PAST FFD PARTICIPANT' ? 'selected' : '' ?>>CURRENT/PAST FFD PARTICIPANT</option>
                                        <option value="Other" <?= $speciality1 === 'Other' ? 'selected' : '' ?>>Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4 mb-3 m-2">
                            <div class="col-12">
                                <!-- <input type="image" src="assets/img/btn-register.png" value="Submit" /> -->
                                <input type="submit" class="reg" style="background-color:#00089a; height:41px;width:100px;color:white" value="Registration" />
                                <a href="./" class="form-cancel"><img src="assets/img/btn-cancel.png" alt="" /></a>
                                <br><br>
                                If already registered, <a href="https://coactx.live/FFD/muktotsav2021//">click here</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <img src="assets/img/bottom-banner.png" class="img-fluid" alt="" />
                </div>
            </div>
        </div>
    </div>


    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script>
        $(function() {
            getCountries();
        });

        function getCountries() {
            $.ajax({
                url: 'control/event.php',
                data: {
                    action: 'getcountries',
                    country: '<?= $country ?>'
                },
                type: 'post',
                success: function(response) {
                    $("#countries").html(response);
                }
            });
        }

        function updateState() {
            var c = $('#country').val();
            if (c != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getstates',
                        country: c
                    },
                    type: 'post',
                    success: function(response) {

                        $("#states").html(response);
                    }
                });
            }
        }

        function updateCity() {
            var s = $('#state').val();
            if (s != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getcities',
                        state: s
                    },
                    type: 'post',
                    success: function(response) {
                        $("#cities").html(response);
                    }
                });
            }
        }  
    </script>

    <?php require_once 'ga.php';  ?>
    <?php require_once 'footer.php';  ?>