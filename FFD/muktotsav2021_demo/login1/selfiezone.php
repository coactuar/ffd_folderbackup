<?php
require_once "logincheck.php";
$curr_room = 'photobooth';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<link rel="stylesheet" href="assets/css/cam.css">
<div class="page-content">
    <div id="content" class="selfiezone">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="selfiezone">
            <img src="assets/img/selfiezone.jpg">
        </div>
        <div id="photobooth">
            <div id="cam-panel" class="camera-panel">
                <div class="photo-jacket-container">
                    <img class="photo-jacket" src="assets/img/selfie/selfie-devendra.png" />
                    <div id="cam-ui">
                        <div id="cam-feed">
                            <video id="cam-feed-video" playsinline autoplay></video>
                            <canvas class="cam-prev"></canvas>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center" id="before-capture" style="display: none;">
                    <button type="button" class="btn btn-capture" id="cam-btn"><i class="fas fa-camera"></i> Take Selfie</button>
                </div>

                <div class="d-flex justify-content-center" id="after-capture" data-html2canvas-ignore style="display: none!important;">
                    <button type="button" class="btn btn-retry" id="retake-btn"><i class="fas fa-undo-alt"></i> Try Again</button>
                    <button type="button" class="btn btn-save" id="save-btn"><i class="fas fa-save"></i> Save Selfie</button>
                </div>

                <div id="social" class="text-center" style="display: none;" data-html2canvas-ignore>
                    <h6>Share on Social Media</h6>
                    <a href="#" id="facebook" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="#" id="linkedin" target="_blank"><i class="fab fa-linkedin"></i></a>
                    <a href="#" id="twitter" target="_blank"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>
                </div>

            </div>

            <!-- <div id="options">

                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('devendra')"> Selfie with Devendra Phadnavis</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('soumya')"> Selfie with Dr. Soumya Swaminathan</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('surya')"> Selfie with L. S. Tejasvi Surya</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('mark')"> Selfie with Mark Tully</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('nitin')"> Selfie with Nitin Gadkari</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('prakash')"> Selfie with Prakash Karat</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('rahul')"> Selfie with Rahul Sir</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('sanjay')"> Selfie with Sanjay Raut</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('shabana')"> Selfie with Shabana Azmi</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('manish')"> Selfie with Shri Manish Tiwari</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('karad')"> Selfie with Shri Vishwanath Karad</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('babul')"> Selfie with Shri. Babul Supriyo</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('jamyang')"> Selfie with Shri. Jamyang Tsering Namgyal</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('mansukh')"> Selfie with Shri. Mansukh Mandaviya</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('rupala')"> Selfie with Shri. Parshottam Rupala</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('raghav')"> Selfie with Shri. Raghav Chadha</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#" onclick="updPB('tiruchi')"> Selfie with Tiruchi Silva</a>
                    </li>
                </ul>


            </div>
        </div> -->
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/CameraController.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.2/FileSaver.js"></script>
<script type="text/javascript" src="assets/js/html2canvas.min.js"></script>
<script>
    $(function() {
        initCamera();
    });

    function updPB(opt) {
        $('.photo-jacket').attr('src', 'assets/img/selfie/selfie-' + opt + '.png');
        $('#cam-ui').removeClass().addClass('layout-' + opt);
    }
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>