<?php
require_once "logincheck.php";
$curr_room = 'youthconnect';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Help Desk Final-2.jpg">
            <a  id="show_talktous" href="#" title="Talk to Us" data-from="<?php echo $_SESSION['userid']; ?>">
  
            </a>
            <div id="lobbyVideo1">
                <iframe src="https://player.vimeo.com/video/653964109?h=65ac62b307" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>
            </div>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>