function include(file) {

    var script = document.createElement('script');
    script.src = file;
    script.type = 'text/javascript';
    script.defer = true;

    document.getElementsByTagName('head').item(0).appendChild(script);

}

/* Include Many js files */
include('js/jquery.min.js');
include('js/bootstrap.min.js');
include('js/jquery.popupoverlay.js');
include('lightbox/html5lightbox.js');
include('js/jquery-ui.js');
include('js/jquery.magnific-popup.min.js');