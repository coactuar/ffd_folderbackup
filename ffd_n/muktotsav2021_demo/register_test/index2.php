<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Registration</title>
    <link rel="stylesheet" href="styles.css">


    <style>

body {
    background-image: url('assets/images/bgbg.png');
    background-position: top center;
    background-repeat: no-repeat;
    background-color: #000044;
}

.imgwel1 {
    width: 50%;
    margin-top: 326px;
    margin-left: 141px;
    margin-bottom: 20px;
}

#form1 {
    /* position: absolute;
    top: 444px;
    left: 700px; */
    width: 100%;
    background-color: #ffffff;
    color: black;
    border-radius: 0px 0px 20px 20px;
    margin-bottom: 4%;
}

.lebel{
    margin-left:4px;
}

legend {
    border-bottom: 5px solid skyblue;
    width: 90%;
    color: darkblue;
    font-size: 22px;
    margin-left: 20px;
    font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
    padding-left: 80px;
}

.btn1 {
    background-image: linear-gradient(to right, #0088c8, #00acd9, #0088c8);
    color: white;
}

.btn2 {
    background-color: #ec3739;
    color: white;
}

.mob {
    font-size: 16px;
}

.email {
    margin-left: 11px;
    width: 96.2%;
}

.program {
    margin-left: 7px;
    width: 98%;
}

.about {
    margin-left: 7px;
    width: 98%;
}

.mg {
    margin-left: 5px;
    margin-right: 5px;
}

.btn {
    margin: 6px;
    margin-left: 5px;
}

.logo {
    /* width: 80%; */
    height: 37px;
}

.bg-image {
    background-image: url('assets/images/bgffdmuk.png');
    height: 100vh;
    background-position: top center;
    background-repeat: no-repeat;
    background-size: 50%;
    /* font-size: 14px; */
    /* margin-top:8px; */
}

@media only screen and (max-width: 1440px) {
    .bg-image {
        background-size: 50%;
    }
}

@media only screen and (max-width: 1024px) {
    .imgwel1 {
        width: 66%;
        margin-top: 292px;
        margin-left: 65px;
        margin-bottom: 17px;
    }
    .bg-image {
        background-size: 52%;
    }
    body {
        background-color: #000044;
        background-image: url('');
    }
    .btn1 {
        font-size: 13.5px;
    }
    .btn2 {
        font-size: 11.55px;
    }
    legend {
        padding-left: 26px;
    }
}

@media only screen and (max-width: 768px) {
    body {
        background-color: #000044;
        background-image: url('');
    }
    .imgwel1 {
        width: 72%;
        margin-top: 292px;
        margin-left: 38px;
        margin-bottom: 17px;
    }
    legend {
        /* font-size: 16px; */
        padding-left: 25px;
    }
    .bg-image {
        background-size: 66%;
    }
    #form1 {
        /* position: absolute;
        top: 444px;
        left: 700px; */
        width: 100%;
        background-color: #ffffff;
        color: black;
        border-radius: 0px 0px 20px 20px;
    }
    .logo {
        /* width: 80%; */
        height: 34px;
    }
    .email {
        width: 93.2%;
    }
    .program {
        width: 96%;
    }
    .about{
        width: 96%;
    }
}

@media only screen and (max-width: 425px) {
    .imgwel1 {
        width: 57%;
        margin-top: 277px;
        margin-left: 88px;
        margin-bottom: 20px;
    }
    .bg-image {
        background-size: 114%;
    }
    /* .form-group {
        margin-bottom: 0px !important;
    } */
    /* * {
        font-size: 10px;
    } */
    /* legend {
        font-size: 14px;
    } */
    .logo {
        height: 37px !important;
        width: 200px !important;
    }
}

@media only screen and (max-width: 375px) {
    .bg-image {
        background-size: 127%;
    }
    legend {
        /* font-size: 15px; */
        padding-left: 50px;
    }
    .imgwel1 {
        width: 58%;
        margin-top: 268px;
        margin-left: 72px;
        margin-bottom: 20px;
    }
}

@media only screen and (max-width: 320px) {
    .bg-image {
        background-size: 140%;
    }
    legend {
        /* font-size: 15px; */
        padding-left: 9px;
    }
    .logo {
        height: 37px !important;
        width: 140px !important;
    }
    .imgwel1 {
        width: 54%;
        margin-top: 261px;
        margin-left: 71px;
        margin-bottom: 20px;
    }
}
    </style>

</head>

<body>
    <div class="container bg-image">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <img src="assets/images/welcomecr.png" class="imgwel1">

            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <form id="form1">
                    <div class="form-row mg">
                        <legend class="m-4">Please fill in the Registration Details</legend>
                        <div class="form-group col-md-6">
                            <label for="inputFirstName">FIRST NAME</label>
                            <input type="text" class="form-control fname" id="inputfname">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLastName">LAST NAME</label>
                            <input type="text" class="form-control lname" id="inputlname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="mg lebel">EMAIL</label>
                        <input type="email" class="form-control email" id="inputEmail">
                    </div>
                    <div class="form-row mg">
                        <div class="form-group col-md-6 ">
                            <label for="inputEmail4">MOBILE NO.</label>
                            <input type="number" class="form-control mobile" id="inputMobile">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputCity">CITY</label>
                            <select id="inputCity" class="form-control">
                            <option selected>Select City</option>
                            <option>...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row mg">
                        <div class="form-group col-md-6">
                            <label for="inputState">STATE</label>
                            <select id="inputState" class="form-control">
                            <option selected>Select State</option>
                            <option>...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputCountry">COUNTRY</label>
                            <select id="inputCountry" class="form-control">
                            <option selected>Select Country</option>
                            <option>...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group mg">
                        <label for="inputProgram" class="lebel">HAVE YOU ATTENDED ANY OF THE FFD PROGRAM</label>
                        <select id="inputProgram" class="form-control program">
                        <option selected>NOT ATTENDED ANY OF PROGRAM</option>
                        <option>FIRST SESSION/FOUNDATION/ONLINE FIRST CLASS</option>
                        <option>INTENSIVE REVERSAL PROGRAM (IRP)</option>
                        <option>TRANCENDENTAL RESIDENSTIAL PROGRAM (TRP)</option>
                        </select>
                    </div>
                    <div class="form-group mg">
                        <label for="inputAbout" class="lebel">FROM WHERE DID YOU COME TO KNOW ABOUT THE EVENT?</label>
                        <select id="inputAbout" class="form-control about">
                        <option>...</option>
                        <option>...</option>
                        </select>
                    </div>
                    <div class="form-row mg">
                        <div class="form-group col-md-6 col-sm-6">
                            <button type="submit" class="btn btn1 mg">REGISTRATION</button>
                            <button type="submit" class="btn btn2 mg">CANCEL</button>
                        </div>
                        <div class="col-md-2 col-sm-2"></div>
                        <div class="form-group col-md-4 col-sm-4">
                            <img src="assets/images/birdffdcr.png" class="logo" alt="logo">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>

    </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>