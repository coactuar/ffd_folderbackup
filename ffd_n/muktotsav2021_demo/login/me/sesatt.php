<?php
require_once "../functions.php";

$session_id = $_GET['s'];

$sess = new Session();
$sess->__set('session_id', $session_id);
$session = $sess->getSession();
$title = $session[0]['session_title'];

$list = $sess->getAttendeesList();
//var_dump($list);
if (!empty($list)) {
  $i = 0;
  foreach ($list as $c) {
    $data[$i]['E-mail ID'] = $c['emailid'];
    $data[$i]['Join Time'] = $c['join_time'];
    $data[$i]['Leave Time'] = $c['leave_time'];

    $i++;
  }

  $filename = $title . "_attendees.xls";
  header("Content-Type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=\"$filename\"");
  ExportFile($data);
}
