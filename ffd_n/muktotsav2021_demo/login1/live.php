<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Live</title>
    <style>
        html,
        body {
            height: 100%;
        }

        * {
            margin: 0;
            padding: 0;
        }

        .video-player {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
    <div id="live-video" class="video-player"></div>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

    <script>
        var player = new Clappr.Player({
            source: "https://dnnzuzbuznubl.cloudfront.net/tristar/live.m3u8",
            parentId: "#live-video",
            width: "100%",
            height: "100%",
            autoplay: true,
            mute: false,
        });

        player.play();
    </script>
</body>

</html>