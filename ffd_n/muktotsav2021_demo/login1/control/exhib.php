<?php
require_once "../inc/config.php";
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];

    switch ($action) {
        case 'delres':
            $resid = $_POST['res_id'];
            $exhib = new Exhibitor();
            $exhib->__set('res_id', $resid);
            $resInfo = $exhib->delResource();

            print_r(json_encode($resInfo));

            break;


        case 'updateFileDLCount':

            $resid = $_POST["resId"];
            $userid = $_POST['userId'];

            $exhib = new Exhibitor();
            $exhib->__set('res_id', $resid);
            $exhib->__set('user_id', $userid);
            $vidupd = $exhib->updateFileDLCount();

            var_dump($vidupd);

            break;
    }
}
