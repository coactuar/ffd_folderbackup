<nav class="navbar bottom-nav">
  <ul class="nav mx-auto">
    
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="audi.php" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="youthconnect.php" title=""><i class="fas fa-network-wired"></i><span class="hide-menu">Help Desk</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="bcsjourney.php" title=""><i class="fa fa-road"></i><span class="hide-menu"> Journey Wall</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="exhibithall.php"><i class="fas fa-border-all"></i><span class="hide-menu">Champions Wall</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="selfiezone.php"><i class="fas fa-camera"></i><span class="hide-menu">Selfie Zone</span></a>
    </li>
    
    <!-- <li class="nav-item">
      <a class="nav-link" href="#" id="selectAudi" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li> -->
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
    <li class="nav-item">
    <audio id="track" controls  autoplay >
  <source src="assets/videos/Overview Audio.mp4"    type="audio/mpeg" />
</audio>

<!-- <div id="player-container">
  <div id="play-pause" class="play">Play</div>
</div> -->
    </div>
    </li>
  </ul>

</nav>

<script>
var track = document.getElementById('track');

var controlBtn = document.getElementById('play-pause');

function playPause() {
    if (track.paused) {
        track.play();
        //controlBtn.textContent = "Pause";
        controlBtn.className = "pause";
    } else { 
        track.pause();
         //controlBtn.textContent = "Play";
        controlBtn.className = "play";
    }
}

controlBtn.addEventListener("click", playPause);
track.addEventListener("ended", function() {
  controlBtn.className = "play";
});
        
    </script>
<!-- <div id="helplines" style="position: absolute;
    bottom: 10px;
    right: 0px;
    text-align: center;
    color: #333;
    width: 11.5%;
    font-size: 0.7rem;">
  For assistance:<br>
  <i class="fas fa-phone-square-alt"></i> +917314-855-655

</div> -->