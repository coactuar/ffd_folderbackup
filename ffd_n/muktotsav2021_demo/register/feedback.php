<?php
require_once 'functions.php';

$succ = false;
$errors = [];

$q01 = '';
$q02 = '';
$q03 = '';
$q04 = '';
$q05 = '';
$q06 = '';
$q07 = '';
$q08 = '';
$q09 = '';
$q10 = '';
$q11 = '';
$q12 = '';
$q13 = '';
$q14 = '';
$q15 = '';
$q16 = '';
// $q17 = '';
// $q18 = '';
// $q19 = '';
// $q20 = '';
// $q21 = '';
// $q22 = '';
// $q23 = '';
// $q24 = '';
// $q25 = '';
$q26 = '';
$q27 = '';
$q28 = '';
// $q29 = '';
$userid = '';

if (isset($_POST['fbsub-btn'])) {

    if (
        empty($_POST['q01']) ||
        empty($_POST['q02']) ||
        empty($_POST['q03']) ||
        empty($_POST['q04']) ||
        empty($_POST['q05']) ||
        empty($_POST['q06']) ||
        empty($_POST['q07']) ||
        empty($_POST['q08']) ||
        empty($_POST['q09']) ||
        empty($_POST['q10']) ||
        empty($_POST['q11']) ||
        empty($_POST['q12']) ||
        empty($_POST['q13']) ||
        empty($_POST['q14']) ||
        empty($_POST['q15']) ||
        empty($_POST['q16']) ||
        // empty($_POST['q17']) ||
        // empty($_POST['q18']) ||
        // empty($_POST['q19']) ||
        // empty($_POST['q20']) ||
        // empty($_POST['q21']) ||
        // empty($_POST['q22']) ||
        // empty($_POST['q23']) ||
        // empty($_POST['q24']) ||
        // empty($_POST['q25']) ||
        empty($_POST['q26']) ||
        empty($_POST['q27']) ||
        empty($_POST['q28']) 
        // empty($_POST['q29'])

    ) {
        $errors['reply'] = 'Please answer all questions.';
    }
    if (isset($_POST['userid'])) {
        $userid = $_POST['userid'];
    } else {
        header('location: ./');
    }

    if (isset($_POST['q01'])) {
        $q01 = $_POST['q01'];
    }
    if (isset($_POST['q02'])) {
        $q02 = $_POST['q02'];
    }
    if (isset($_POST['q03'])) {
        $q03 = $_POST['q03'];
    }
    if (isset($_POST['q04'])) {
        $q04 = $_POST['q04'];
    }
    if (isset($_POST['q05'])) {
        $q05 = $_POST['q05'];
    }
    if (isset($_POST['q06'])) {
        $q06 = $_POST['q06'];
    }
    if (isset($_POST['q07'])) {
        $q07 = $_POST['q07'];
    }
    if (isset($_POST['q08'])) {
        $q08 = $_POST['q08'];
    }
    if (isset($_POST['q09'])) {
        $q09 = $_POST['q09'];
    }
    if (isset($_POST['q10'])) {
        $q10 = $_POST['q10'];
    }
    if (isset($_POST['q11'])) {
        $q11 = $_POST['q11'];
    }
    if (isset($_POST['q12'])) {
        $q12 = $_POST['q12'];
    }
    if (isset($_POST['q13'])) {
        $q13 = $_POST['q13'];
    }
    if (isset($_POST['q14'])) {
        $q14 = $_POST['q14'];
    }
    if (isset($_POST['q15'])) {
        $q15 = $_POST['q15'];
    }
    if (isset($_POST['q16'])) {
        $q16 = $_POST['q16'];
    }
    if (isset($_POST['q17'])) {
        $q17 = $_POST['q17'];
    }
    if (isset($_POST['q18'])) {
        $q18 = $_POST['q18'];
    }
    if (isset($_POST['q19'])) {
        $q19 = $_POST['q19'];
    }
    if (isset($_POST['q20'])) {
        $q20 = $_POST['q20'];
    }
    if (isset($_POST['q21'])) {
        $q21 = $_POST['q21'];
    }
    if (isset($_POST['q22'])) {
        $q22 = $_POST['q22'];
    }
    if (isset($_POST['q23'])) {
        $q23 = $_POST['q23'];
    }
    if (isset($_POST['q24'])) {
        $q24 = $_POST['q24'];
    }
    if (isset($_POST['q25'])) {
        $q25 = $_POST['q25'];
    }
    if (isset($_POST['q26'])) {
        $q26 = $_POST['q26'];
    }
    if (isset($_POST['q27'])) {
        $q27 = $_POST['q27'];
    }
    if (isset($_POST['q28'])) {
        $q28 = $_POST['q28'];
    }
    if (isset($_POST['q29'])) {
        $q29 = $_POST['q29'];
    }



    if (count($errors) == 0) {
        $fb = new Feedback();
        $fb->__set('user_id', $userid);
        $fb->__set('q01', $_POST['q01']);
        $fb->__set('q02', $_POST['q02']);
        $fb->__set('q03', $_POST['q03']);
        $fb->__set('q04', $_POST['q04']);
        $fb->__set('q05', $_POST['q05']);
        $fb->__set('q06', $_POST['q06']);
        $fb->__set('q07', $_POST['q07']);
        $fb->__set('q08', $_POST['q08']);
        $fb->__set('q09', $_POST['q09']);
        $fb->__set('q10', $_POST['q10']);
        $fb->__set('q11', $_POST['q11']);
        $fb->__set('q12', $_POST['q12']);
        $fb->__set('q13', $_POST['q13']);
        $fb->__set('q14', $_POST['q14']);
        $fb->__set('q15', $_POST['q15']);
        $fb->__set('q16', $_POST['q16']);
        // $fb->__set('q17', $_POST['q17']);
        // $fb->__set('q18', $_POST['q18']);
        // $fb->__set('q19', $_POST['q19']);
        // $fb->__set('q20', $_POST['q20']);
        // $fb->__set('q21', $_POST['q21']);
        // $fb->__set('q22', $_POST['q22']);
        // $fb->__set('q23', $_POST['q23']);
        // $fb->__set('q24', $_POST['q24']);
        // $fb->__set('q25', $_POST['q25']);
       $fb->__set('q26', $_POST['q26']);
        $fb->__set('q27', $_POST['q27']);
        $fb->__set('q28', $_POST['q28']);
        // $fb->__set('q29', $_POST['q29']);

        $subFeedback = $fb->submitFeedback();

        //var_dump($subFeedback);

        if ($subFeedback['status'] == 'success') {
            $succ = true;
        } else {
            $errors['msg'] = $subFeedback['message'];
        }
    }
}



?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $event_title ?></title>
    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css" />
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <img src="assets/img/reg-banner.png" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row bg-white color-grey py-2">
            <div class="col-12 text-center">
                <h5 class="reg-title">Best of AACE 2021-Feedback Form</h5>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 offset-md-2">
                <?php if (!$succ) { ?>
                    <div id="register-area">
                        <?php
                        if (count($errors) > 0) : ?>
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    <?php foreach ($errors as $error) : ?>
                                        <li>
                                            <?php echo $error; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif;
                        ?>
                        <form method="POST">
                            <input type="hidden" id="userid" name="userid" class="input" value="<?= $_SESSION['userid'] ?>" required>


                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>Thank you for attending the ‘Best of AACE 2021’ virtual symposia. Please let us know about your experience of this symposia and feedback for the betterment of future educational events.</strong>
                                </div>
                            </div>
                            <div class="row mb-1 mt-3">
                                <div class="col-12">
                                    <strong>1.	Please rate the overall aspects of this educational activity based on: 
(1=Poor 2=Below Average 3=Average 4=Above average 5=Outstanding) 
</strong>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <ul class="list-unstyled">
                                        <li><input type="radio" name="q26" <?= ($q26 == 'Educational content') ? 'checked' : '' ?> value="Educational content"> Educational content</li>
                                        <li><input type="radio" name="q26" <?= ($q26 == 'Relevance to practice') ? 'checked' : '' ?> value="Relevance to practice">	Relevance to practice </li>
                                        <li><input type="radio" name="q26" <?= ($q26 == 'Questions and discussions') ? 'checked' : '' ?> value="Questions and discussions "> Questions and discussions </li>
                                        <li><input type="radio" name="q26" <?= ($q26 == 'Quality of presenters ') ? 'checked' : '' ?> value="Quality of presenters "> Quality of presenters </li>
                                        <li><input type="radio" name="q26" <?= ($q26 == 'Selection of topics ') ? 'checked' : '' ?> value="Selection of topics ">	Selection of topics  </li>
                                        <li><input type="radio" name="q26" <?= ($q26 == 'Overall quality of activity ') ? 'checked' : '' ?> value="Overall quality of activity ">	Overall quality of activity  </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row mb-1 mt-3">
                                <div class="col-12">
                                    <strong>2.	Please rate the faculty's quality of teaching (knowledgeable, organized and
effective in his/her presentation)

</strong>
                                </div>
                            </div>
                         
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>Please rate the faculty's quality of teaching.</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <th align="left">Faculty</td>
                                            <th width="150">Excellent</td>
                                            <th width="100">Good</td>
                                            <th width="100">Fair</td>
                                            <th width="150">Poor</td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Betul Hatipoglu 
(Triglycerides: Medications and Trials)
</td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Rita Basu
(Endocrinologic Approach to 
NAFLD/NASH: Evaluation and Management,
When to Refer to a Hepatologist)
</td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Dennis Bruemmer 
(Hyperlipidemia in Young Patients with Type 1 Diabetes: Review of Current Guidelines and Evidence)
</td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Spyridoula Maraka 
(Managing Thyroid Disease in Pregnancy)
</td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Anand Vaidya 
(Evaluation and Management of Suspicious Adrenal Masses: Approach to the Adrenal Incidentaloma)
</td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q09" <?= ($q09 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Natalie Cusano 
(Mild Forms of Hyperparathyroidism [Including Normocalcemic]: What's an
Endocrinologist To Do?)
</td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q10" <?= ($q10 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Richard Auchus
(Approach to Patient with Adrenal Insufficiency)
</td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q11" <?= ($q11 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Maria Fleseriu 
(Personalized Treatment in Acromegaly)
</td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Excellent') ? 'checked' : '' ?> value="Excellent"></td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Good') ? 'checked' : '' ?> value="Good"></td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Fair') ? 'checked' : '' ?> value="Fair"></td>
                                            <td><input type="radio" name="q12" <?= ($q12 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                        </tr>
                          
                                    </table>
                                </div>
                            </div>
 <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <th align="left">Faculty</td>
                                            <th width="150">Change the management and/or treatment of my patients</td>
                                            <th width="150">It validated my current practice</td>
                                            <th width="100">I will not make any changes to my practice

</td>
                                            <th width="100">Other, please specify:</td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Betul Hatipoglu 
(Triglycerides: Medications and Trials)
</td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Rita Basu
(Endocrinologic Approach to 
NAFLD/NASH: Evaluation and Management,
When to Refer to a Hepatologist)
 .</td>
                                            <!-- <td><input type="radio" name="q02" <?= ($q02 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                      -->
                                      <td><input type="radio" name="q02" <?= ($q02 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Dennis Bruemmer 
(Hyperlipidemia in Young Patients with Type 1 Diabetes: Review of Current Guidelines and Evidence)
.</td>
                                            <!-- <td><input type="radio" name="q03" <?= ($q03 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                       -->
                                       <td><input type="radio" name="q03" <?= ($q03 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Dr Spyridoula Maraka 
(Managing Thyroid Disease in Pregnancy)
</td>
                                            <!-- <td><input type="radio" name="q04" <?= ($q04 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                      -->
                                      <td><input type="radio" name="q04" <?= ($q04 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Evaluation and Management of Suspicious Adrenal Masses: Approach to the Adrenal Incidentaloma
</td>
                                            <!-- <td><input type="radio" name="q13" <?= ($q13 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                       -->
                                       <td><input type="radio" name="q13" <?= ($q13 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q13" <?= ($q13 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Mild Forms of Hyperparathyroidism [Including Normocalcemic]: What's an
Endocrinologist To Do?

</td>
                                            <!-- <td><input type="radio" name="q14" <?= ($q14 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        -->
                                        <td><input type="radio" name="q14" <?= ($q14 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Approach to Patient with Adrenal Insufficiency
</td>
                                            <!-- <td><input type="radio" name="q15" <?= ($q15 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        -->
                                        <td><input type="radio" name="q15" <?= ($q15 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Personalized Treatment in Acromegaly
</td>
                                            <!-- <td><input type="radio" name="q16" <?= ($q16 == 'Strongly Agree') ? 'checked' : '' ?> value="Strongly Agree"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Strongly Disagree') ? 'checked' : '' ?> value="Strongly Disagree"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Agree') ? 'checked' : '' ?> value="Agree"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Disagree') ? 'checked' : '' ?> value="Disagree"></td>
                                        -->
                                        <td><input type="radio" name="q16" <?= ($q16 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'It validated my current practice') ? 'checked' : '' ?> value="It validated my current practice"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Other, please specify') ? 'checked' : '' ?> value="Other, please specify"></td>
                                 
                                        </tr>
                                    </table>
                                </div>
                            </div>
                         
                       
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>4.	What influenced you to attend this meeting?</strong>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <ul class="list-unstyled">
                                        <li><input type="radio" name="q27" <?= ($q27 == 'Programme description through promotional videos and teasers/inputs') ? 'checked' : 'Programme description through promotional videos and teasers/inputs' ?> value="Generalist"> Programme description through promotional videos and teasers/inputs</li>
                                        <li><input type="radio" name="q27" <?= ($q27 == 'List of faculties') ? 'checked' : '' ?> value="List of faculties"> List of faculties</li>
                                        <li><input type="radio" name="q27" <?= ($q27 == 'List of topics') ? 'checked' : '' ?> value="List of topics"> 	List of topics</li>
                                        <li><input type="radio" name="q27" <?= ($q27 == 'Banner under which it is conducted (American Association of Clinical Endocrinology)') ? 'checked' : '' ?> value="Banner under which it is conducted (American Association of Clinical Endocrinology)"> Banner under which it is conducted (American Association of Clinical Endocrinology)</li>
                                            </ul>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-12">
                                    <strong>5.	Do you have any other feedback?</strong>
                                    <br>
                                    <textarea name="q28" id="q28" rows="4" class="input"><?= $q28 ?></textarea>
                                </div>
                            </div>
               

                            <div class="form-group">
                                <input type="submit" name="fbsub-btn" id="btnSubmit" class="btn btn-primary" value="Submit">
                            </div>


                        </form>
                    </div>
                <?php } else { ?>
                    <div id="registration-confirmation">
                        <div class="alert alert-success">
                            Thanks for giving us your valuable feedback!<br>
                        </div>

                    </div>
                <?php } ?>

            </div>
        </div>
        <div class="row bg-white">
            <div class="col-12">
                <img src="assets/img/line-h.jpg" class="img-fluid" alt="" />
            </div>
        </div>
        <!-- <div class="row bg-white p-2">
            <div class="col-4 bor-right p-2 text-center">
                <img src="assets/img/in-assoc.png" class="img-fluid bot-img" alt="" />
            </div>
            <div class="col-4 p-2 text-center">
                <img src="assets/img/sci-partner.png" class="img-fluid bot-img" alt="" />
            </div>
            <div class="col-4 bor-left p-2 text-center color-grey">
                <img src="assets/img/brought-by.png" class="img-fluid bot-img" alt="" />
                <div class="visit">
                    Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
            </div> -->
        </div>


    </div>
    <!-- <div id="code">IPL/O/BR/09042021</div> -->

    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <?php require_once 'ga.php';  ?>
    <?php require_once 'footer.php';  ?>